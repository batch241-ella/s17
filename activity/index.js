/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function displayUserInfo() {
		let fullName = prompt("What is your name?");
		let age = prompt("What is your age?");
		let location = prompt("Where do you live?");
		alert("Thank you for your input!");

		console.log("Hello, " + fullName);
		console.log("You are " + age + " years old.");
		console.log("You live in " + location);
	}

	displayUserInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function displayFavouriteBands() {
		console.log("1. Dayglow");
		console.log("2. Hiroyuki Sawano");
		console.log("3. Coldplay");
		console.log("4. Bump of Chicken");
		console.log("5. Kessoku Band");
	}

	displayFavouriteBands();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	/*
		Inglorius Basterds - 89 / 88
		The Grand Budapest Hotel - 92 / 86
		Coraline - 90 / 74
		How to Train Your Dragin - 99 / 91
		Your Name - 98 / 94
	*/
	
	//third function here:
	function displayFavouriteMovies() {
		console.log("1. Inglorius Basterds");
		console.log("Rotten Tomatoes Rating: 89%");

		console.log("2. The Grand Budapest Hotel");
		console.log("Rotten Tomatoes Rating: 92%");

		console.log("3. Coraline");
		console.log("Rotten Tomatoes Rating: 90%");

		console.log("4. How to Train Your Dragon");
		console.log("Rotten Tomatoes Rating: 99%");

		console.log("5. Your Name");
		console.log("Rotten Tomatoes Rating: 98%");
	}

	displayFavouriteMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");

	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();

// console.log(friend1);
// console.log(friend2);